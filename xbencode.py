#!/usr/bin/env python2

# COPYRIGHT: 2019 <txpower@muc.ccc.de>
# LICENSE: GNU GPLv2 or later
# DESCRIPTION: simple encoder and decoder for a BEncode-compatible serialization
#              format with more data types: Null/None, Boolean, Fraction, Set
# BUGS:
#       * recursion, decode("l"*65536+"e"*65536) fails because of that
#       * Python2-only

import StringIO
import sys
from fractions import Fraction
if sys.version_info.major > 2:
  raise BaseException("Python3 not supported (yet)")

def _to_fraction(x):
  if type(x) is Fraction:
    return x
  elif type(x) is float:
    return Fraction.from_float(x)
  else:
    raise TypeError("only float can be converted to fraction")

def _read_to(io,delim):
  out=""
  char=io.read(1)
  while not char in delim:
    out+=char
    char=io.read(1)
  return out

def encode(data,only_str_keys=True):
  if data is None:
    return "n"
  elif data is False:
    return "f"
  elif data is True:
    return "t"
  elif type(data) in [int,long]:
    return "i"+bytes(data)+"e"
  elif type(data) in [float,Fraction]:
    f=Fraction(data)
    return "r"+bytes(f.numerator)+"/"+bytes(f.denominator)+"e"
  elif type(data) is bytes:
    return bytes(len(data))+":"+data
  elif type(data) is unicode:
    return encode(data.encode("utf8"),only_str_keys)
  elif type(data) is list:
    out="l"
    for element in data:
      out+=encode(element,only_str_keys=only_str_keys)
    return out+"e"
  elif type(data) is dict:
    out="d"
    keys=list(data.keys()) #python3 needs conversion
    keys.sort()
    for key in keys:
      if (only_str_keys is False) or (type(key) in [bytes,unicode]):
        out+=encode(key,only_str_keys=only_str_keys)+encode(data[key],only_str_keys=only_str_keys)
      else:
        raise TypeError("key must be a str, but is "+str(type(key)))
    return out+"e"
  elif type(data) is set:
    l=list(data)
    l.sort()
    out="s"
    for element in data:
      out+=encode(element,only_str_keys=only_str_keys)
    return out+"e"
  else:
    raise TypeError("data must be one of (None,bool,int,float,str,list,dict,set), but is "+str(type(data)))

def decode(io,char="",only_str_keys=True,fix_dict=False):
  if type(io) is str:
    io=StringIO.StringIO(io)
  if char == "":
   char=io.read(1)
  if char == "n":
    return None
  elif char == "f":
    return False
  elif char == "t":
    return True
  elif char == "i":
    return long(_read_to(io,"e"))
  elif char == "r":
    n=long(_read_to(io,"/"))
    d=long(_read_to(io,"e"))
    return Fraction(n,d)
  elif char in "0123456789":
    strlen=int(char+_read_to(io,":"))
    out=io.read(strlen)
    if strlen == len(out):
      return out
    else:
      raise BaseException("unexpected EOF")
  elif char == "l":
    out=[]
    char=io.read(1)
    while not char == "e":
      out.append(decode(io,char=char,only_str_keys=only_str_keys,fix_dict=fix_dict))
      char=io.read(1)
    return out
  elif char == "s":
    out=[]
    char=io.read(1)
    while not char == "e":
      out.append(decode(io,char=char,only_str_keys=only_str_keys,fix_dict=fix_dict))
      char=io.read(1)
    return set(out)
  elif char == "d":
    out={}
    char=io.read(1)
    oldkey=None
    while not char == "e":
      key=decode(io,char=char,only_str_keys=only_str_keys,fix_dict=fix_dict)
      if not type(key) is str and only_str_keys:
        raise TypeError("key must be a str, but is "+bytes(type(key)))
      elif (oldkey>=key) and not fix_dict:
        raise BaseException("keys must be sorted")
      else:
        char=io.read(1)
        out[key]=decode(io,char=char,only_str_keys=only_str_keys,fix_dict=fix_dict)
      oldkey=key
      char=io.read(1)
    return out
  else:
    raise TypeError("unrecognized type: "+char)
